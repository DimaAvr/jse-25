package ru.tsc.avramenko.tm.exception.system;

import ru.tsc.avramenko.tm.command.system.HelpCommand;
import ru.tsc.avramenko.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    private static String help = new HelpCommand().name();

    public UnknownCommandException() {
        super("Incorrect command! Use `" + help + "` for display list of terminal commands.");
    }

}