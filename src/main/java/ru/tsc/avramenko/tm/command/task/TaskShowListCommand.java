package ru.tsc.avramenko.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractTaskCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.enumerated.Sort;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskShowListCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show a list of tasks.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sort = TerminalUtil.nextLine();
        @NotNull List<Task> tasks;
        @NotNull final String userId = serviceLocator.getAuthService().getCurrentUserId();
        if (sort == null || sort.isEmpty()) tasks = serviceLocator.getTaskService().findAll(userId);
        else {
            @NotNull Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = serviceLocator.getTaskService().findAll(userId, sortType.getComparator());
        }
        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task.toString());
            index++;
        }
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}