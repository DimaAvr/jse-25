package ru.tsc.avramenko.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.command.AbstractUserCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;

public class UserClearCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "user-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Clear all users.";
    }

    @Override
    public void execute() {
        serviceLocator.getUserService().clear();
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}